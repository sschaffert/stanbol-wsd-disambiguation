package at.salzburgresearch.stanbol.engines.wsd.api;

import at.salzburgresearch.stanbol.engines.wsd.model.Suggestion;
import org.apache.clerezza.rdf.core.UriRef;

/**
 * A service allowing to calculate or look up the relatedness of two concepts. Relatedness is a measure of how
 * related two concepts are and is always a dpuble value between 0 and 1, where higher values mean higher relatedness.
 * <p/>
 * Implementations can either compute the relatedness at runtime or look it up in a pre-computed index.
 *
 * @author Sebastian Schaffert (sschaffert@apache.org)
 */
public interface RelatednessService {

    public final static String PROPERTY_NAME = "org.apache.stanbol.enhancer.engines.wsd_disambiguation.relatedness.name";

    /**
     * Return the relatedness between the two concepts identified by conceptUri1 and conceptUri2 as a value between
     * 0 and 1. Higher values mean higher relatedness, a value of 0 means "not related at all".
     *
     *
     * @param conceptUri1 URI of the first concept
     * @param conceptUri2 URI of the second concept
     * @return a value between 0 and 1 representing how related the two concepts are
     */
    public double getRelatedness(Suggestion conceptUri1, Suggestion conceptUri2);

}
