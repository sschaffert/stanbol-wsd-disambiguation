package at.salzburgresearch.stanbol.engines.wsd.engine;

import at.salzburgresearch.stanbol.engines.wsd.api.RelatednessService;
import at.salzburgresearch.stanbol.engines.wsd.model.DisambiguationData;
import at.salzburgresearch.stanbol.engines.wsd.model.ExtractedEntity;
import at.salzburgresearch.stanbol.engines.wsd.model.RelatednessGraph;
import at.salzburgresearch.stanbol.engines.wsd.model.Suggestion;
import at.salzburgresearch.stanbol.engines.wsd.services.GoogleRelatednessService;
import at.salzburgresearch.stanbol.engines.wsd.services.LevensteinRelatednessService;
import org.apache.clerezza.rdf.core.LiteralFactory;
import org.apache.clerezza.rdf.core.MGraph;
import org.apache.clerezza.rdf.core.Triple;
import org.apache.clerezza.rdf.core.UriRef;
import org.apache.clerezza.rdf.core.impl.TripleImpl;
import org.apache.felix.scr.annotations.Activate;
import org.apache.felix.scr.annotations.Component;
import org.apache.felix.scr.annotations.ConfigurationPolicy;
import org.apache.felix.scr.annotations.Deactivate;
import org.apache.felix.scr.annotations.Property;
import org.apache.felix.scr.annotations.Reference;
import org.apache.felix.scr.annotations.Service;
import org.apache.stanbol.enhancer.servicesapi.ContentItem;
import org.apache.stanbol.enhancer.servicesapi.EngineException;
import org.apache.stanbol.enhancer.servicesapi.EnhancementEngine;
import org.apache.stanbol.enhancer.servicesapi.InvalidContentException;
import org.apache.stanbol.enhancer.servicesapi.ServiceProperties;
import org.apache.stanbol.enhancer.servicesapi.helper.ContentItemHelper;
import org.apache.stanbol.enhancer.servicesapi.helper.EnhancementEngineHelper;
import org.apache.stanbol.enhancer.servicesapi.impl.AbstractEnhancementEngine;
import org.apache.stanbol.enhancer.servicesapi.rdf.NamespaceEnum;
import org.osgi.framework.InvalidSyntaxException;
import org.osgi.service.cm.ConfigurationException;
import org.osgi.service.component.ComponentContext;
import org.osgi.util.tracker.ServiceTracker;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Dictionary;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import static org.apache.stanbol.enhancer.servicesapi.rdf.Properties.DC_RELATION;
import static org.apache.stanbol.enhancer.servicesapi.rdf.Properties.ENHANCER_CONFIDENCE;

/**
 * Implements a disambiguation using the indegree centrality as described in a paper by Sinha and Mihalcea (2006).
 * The centrality algorithm used is a simple indegree algorithm that computes for each vertice the sum of the weight
 * of all adjacent edges. The weights are determined by a relatedness service which returns a measure for the
 * relatedness between two concepts.
 *
 * <p/>
 * See also <a href="http://www.cse.unt.edu/~rada/papers/sinha.ieee07.pdf">http://www.cse.unt.edu/~rada/papers/sinha.ieee07.pdf</a>
 *
 * @author Sebastian Schaffert (sschaffert@apache.org)
 */
@Component(
        configurationFactory = true,
        policy = ConfigurationPolicy.OPTIONAL,
        specVersion = "1.1",
        metatype = true,
        immediate = true,
        inherit = true)
@Service
@org.apache.felix.scr.annotations.Properties(value = {@Property(name = EnhancementEngine.PROPERTY_NAME, value = "disambiguation-wsd")})
public class CentralityDisambiguationEngine extends AbstractEnhancementEngine<RuntimeException,RuntimeException> implements ServiceProperties {

    @Property(intValue = 5)
    public static final String WINDOW_SIZE = "org.apache.stanbol.enhancer.engines.wsd_disambiguation.window_size";

    @Property
    public static final String RELATEDNESS = "org.apache.stanbol.enhancer.engines.wsd_disambiguation.relatedness";

    private static Logger log = LoggerFactory.getLogger(CentralityDisambiguationEngine.class);

    private ServiceTracker relatednessTracker;

    private int maxDist;

    /**
     * The {@link org.apache.clerezza.rdf.core.LiteralFactory} used to create typed RDF literals
     */
    private final LiteralFactory literalFactory = LiteralFactory.getInstance();



    @SuppressWarnings("unchecked")
    @Activate
    protected void activate(ComponentContext context) throws ConfigurationException {
        super.activate(context);
        Dictionary<String,Object> config = context.getProperties();

        if(config.get(WINDOW_SIZE) != null) {
            maxDist = Integer.parseInt(config.get(WINDOW_SIZE).toString());
        } else {
            maxDist = 5;
        }

        String filter = String.format("(&(objectClass=%s)(%s=%s))", RelatednessService.class.getName(),RelatednessService.PROPERTY_NAME, config.get(RELATEDNESS));
        //String filter = String.format("(%s=%s)", EnhancementEngine.PROPERTY_NAME, config.get(RELATEDNESS));

        log.info("relatedness service query: {}", filter);

        try {
            relatednessTracker = new ServiceTracker(context.getBundleContext(),context.getBundleContext().createFilter(filter), null);
            relatednessTracker.open();
        } catch (InvalidSyntaxException e) {
            log.error("could not initialise service tracker for relatedness service (syntax error in query)");
        }
    }

    @Deactivate
    protected void deactivate(ComponentContext context) {
        if(relatednessTracker != null) {
            relatednessTracker.close();
        }
    }

    /**
     * Getter for the properties defined by this service.
     *
     * @return An unmodifiable map of properties defined by this service
     */
    @Override
    public Map<String, Object> getServiceProperties() {
        return Collections.<String,Object>singletonMap(ServiceProperties.ENHANCEMENT_ENGINE_ORDERING, ServiceProperties.ORDERING_POST_PROCESSING);
    }

    /**
     * Indicate if this engine can enhance supplied ContentItem, and if it
     * suggests enhancing it synchronously or asynchronously. The
     * {@link org.apache.stanbol.enhancer.servicesapi.EnhancementJobManager} can force sync/async mode if desired, it is
     * just a suggestion from the engine.
     *
     * @throws org.apache.stanbol.enhancer.servicesapi.EngineException
     *          if the introspecting process of the content item
     *          fails
     */
    @Override
    public int canEnhance(ContentItem ci) throws EngineException {
        // check if relatedness service is present
        if(relatednessTracker.getService() == null) {
            log.error("relatedness service is not available; cannot enhance content");
            return CANNOT_ENHANCE;
        }


        // check if content is present
        try {
            if ((ContentItemHelper.getText(ci.getBlob()) == null)
                    || (ContentItemHelper.getText(ci.getBlob()).trim().isEmpty())) {
                return CANNOT_ENHANCE;
            }
        } catch (IOException e) {
            log.error("Failed to get the text for " + "enhancement of content: " + ci.getUri(), e);
            throw new InvalidContentException(this, ci, e);
        }
        // default enhancement is synchronous enhancement
        return ENHANCE_SYNCHRONOUS;
    }

    /**
     * Implements a disambiguation using the indegree centrality as described in a paper by Sinha and Mihalcea (2006).
     * The centrality algorithm used is a simple indegree algorithm that computes for each vertice the sum of the weight
     * of all adjacent edges. The weights are determined by a relatedness service which returns a measure for the
     * relatedness between two concepts.
     *
     * <p/>
     * See also <a href="http://www.cse.unt.edu/~rada/papers/sinha.ieee07.pdf">http://www.cse.unt.edu/~rada/papers/sinha.ieee07.pdf</a>
     */
    @Override
    public void computeEnhancements(ContentItem ci) throws EngineException {
        // get disambiguation data from content item as starting point for computation
        DisambiguationData data;

        ci.getLock().readLock().lock();
        try {
            data = DisambiguationData.createFromContentItem(ci);
        } finally {
            ci.getLock().readLock().unlock();
        }

        RelatednessGraph graph = buildRelatednessGraph(data);

        assignSuggestionScores(data, graph);


        ci.getLock().writeLock().lock();
        try {
            applyDisambiguationResults(ci.getMetadata(), data);
        } finally {
            ci.getLock().writeLock().unlock();
        }
    }


    /**
     * Build a relatedness graph using the given disambiguation data. The maxDist field defines the size of the "window"
     * to use for computing the graph. The relatedness service allows looking up the relatedness measure between two concepts.
     * <p/>
     * See also <a href="http://www.cse.unt.edu/~rada/papers/sinha.ieee07.pdf">http://www.cse.unt.edu/~rada/papers/sinha.ieee07.pdf</a>
     *
     * @param data
     */
    protected RelatednessGraph buildRelatednessGraph(DisambiguationData data) throws EngineException {
        RelatednessGraph graph = new RelatednessGraph();

        RelatednessService relatednessService = (RelatednessService) relatednessTracker.getService();

        if(relatednessService != null) {
            // build graph of label dependencies (see http://www.cse.unt.edu/~rada/papers/sinha.ieee07.pdf)
            ArrayList <ExtractedEntity> list = new ArrayList<ExtractedEntity>(data.directoryTextAnotation);
            for(int i = 0; i<list.size(); i++) {
                for(int j = i+1; j<list.size(); j++) {
                    if(j-i > maxDist) {
                        break;
                    }
                    for(Suggestion si : list.get(i).getSuggestions()) {
                        if(!graph.containsVertex(si)) {
                            graph.addVertex(si);
                        }
                        for(Suggestion sj : list.get(j).getSuggestions()) {
                            if(!graph.containsVertex(sj)) {
                                graph.addVertex(sj);
                            }
                            double weight = relatednessService.getRelatedness(si, sj);
                            if(weight > 0.0) {
                                graph.addEdge(si, weight, sj);
                            }
                        }
                    }
                }

            }

            log.info("built relatedness graph with {} edges", graph.getSize());

            return graph;
        } else {
            throw new EngineException("no relatedness service provider found");
        }
    }

    /**
     * Compute and assign the suggestion scores for each suggestion in the disambiguation data. Uses the relatedness
     * graph passed as argument to compute the centrality of each suggestion and assigns this value as score for the
     * suggestion.
     *
     * @param data
     */
    protected void assignSuggestionScores(DisambiguationData data, RelatednessGraph graph) {
        for(ExtractedEntity entity : data.directoryTextAnotation) {
            for(Suggestion suggestion : entity.getSuggestions()) {
                suggestion.setDisambiguatedConfidence(graph.computeCentrality(suggestion));

                log.info("text annotation {}: suggestion {} with score {}", new Object[] {entity.getName(), suggestion.getEntityUri(), suggestion.getDisambiguatedConfidence()});
            }
        }
    }

    /**
     * Adds the disambiguation results to the enhancement structure
     *
     * @param graph
     *            the metadata of the {@link ContentItem}
     * @param disData
     *            the disambiguation data
     */
    protected void applyDisambiguationResults(MGraph graph, DisambiguationData disData) {
        // TODO: we should use the normal confidence property in the future!
        UriRef ENHANCER_CONFIDENCE = new UriRef("http://www.salzburgresearch.at/disambiguation");

        for (ExtractedEntity savedEntity : disData.textAnnotations.values()) {
            for (Suggestion s : savedEntity.getSuggestions()) {
                if (s.getDisambiguatedConfidence() != null) {
                    if (disData.suggestionMap.get(s.getEntityAnnotation()).size() > 1) {
                        // already encountered AND disambiguated -> we need to clone!!
                        log.info("clone {} suggesting {} for {}[{},{}]({})",
                                new Object[] {s.getEntityAnnotation(), s.getEntityUri(), savedEntity.getName(),
                                        savedEntity.getStart(), savedEntity.getEnd(), savedEntity.getUri()});
                        s.setEntityAnnotation(cloneTextAnnotation(graph, s.getEntityAnnotation(),
                                savedEntity.getUri()));
                        log.info("  - cloned {}", s.getEntityAnnotation());
                    }
                    // change the confidence
                    EnhancementEngineHelper.set(graph, s.getEntityAnnotation(), ENHANCER_CONFIDENCE, s.getDisambiguatedConfidence(), literalFactory);
                    EnhancementEngineHelper.addContributingEngine(graph, s.getEntityAnnotation(), this);
                }
            }
        }
    }

    /**
     * This creates a 'clone' of the fise:EntityAnnotation where the original does no longer have a
     * dc:relation to the parsed fise:TextAnnotation and the created clone does only have a dc:relation to the
     * parsed fise:TextAnnotation.
     * <p>
     * This is required by disambiguation because other engines typically only create a single
     * fise:EntityAnnotation instance if several fise:TextAnnotation do have the same fise:selected-text
     * values. So for a text that multiple times mentions the same Entity (e.g. "Paris") there will be
     * multiple fise:TextAnnotations selecting the different mentions of that Entity, but there will be only a
     * single set of suggestions - fise:EntityAnnotations (e.g. "Paris, France" and "Paris, Texas"). Now lets
     * assume a text like
     *
     * <pre>
     *     Paris is the capital of France and it is worth a visit for sure. But
     *     one can also visit Paris without leaving the United States as there
     *     is also a city with the same name in Texas.
     * </pre>
     *
     * Entity Disambiguation need to be able to have different fise:confidence values for the first and second
     * mention of Paris and this is only possible of the fise:TextAnnotations of those mentions do NOT refer
     * to the same set of fise:EntityAnnotations.
     * <p>
     * This methods accomplished exactly that as it
     * <ul>
     * <li>creates a clone of a fise:EntityAnnotation
     * <li>removes the dc:relation link to the 2nd mention of Paris from the original
     * <li>only adds the dc:relation of the end mention to the clone
     * </ul>
     * So in the end you will have two fise:EntityAnnotation
     * <ul>
     * <li>the original fise:EntityAnnotation with dc:relation to all fise:TextAnnotations other than the 2nd
     * mention (the one this method was called for)
     * <li>the cloned fise:EntityAnnnotation with a dc:relation to the 2nd mention.
     * </ul>
     *
     * @param graph
     * @param entityAnnotation
     * @param textAnnotation
     * @return
     */
    public static UriRef cloneTextAnnotation(MGraph graph, UriRef entityAnnotation, UriRef textAnnotation) {
        UriRef copy = new UriRef("urn:enhancement-" + EnhancementEngineHelper.randomUUID());
        Iterator<Triple> it = graph.filter(entityAnnotation, null, null);
        // we can not add triples to the graph while iterating. So store them
        // in a list and add later
        List<Triple> added = new ArrayList<Triple>(32);
        while (it.hasNext()) {
            Triple triple = it.next();
            if (DC_RELATION.equals(triple.getPredicate())) {
                if (triple.getObject().equals(textAnnotation)) {
                    // remove the dc relation to the currently processed
                    // textAnnotation from the original
                    it.remove();
                    // and add it to the copy
                    added.add(new TripleImpl(copy, // use the copy as subject!
                            triple.getPredicate(), triple.getObject()));
                } // else it is not the currently processed TextAnnotation
                // so we need to keep in in the original and NOT add
                // it to the copy
            } else { // we can copy all other information 1:1
                added.add(new TripleImpl(copy, // use the copy as subject!
                        triple.getPredicate(), triple.getObject()));
            }
        }
        graph.addAll(added);
        return copy;
    }

}
