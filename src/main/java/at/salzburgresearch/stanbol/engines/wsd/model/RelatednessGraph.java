package at.salzburgresearch.stanbol.engines.wsd.model;

import org.apache.commons.graph.model.UndirectedMutableGraph;

/**
 * A representation of a weighted undirected graph
 *
 * @author Sebastian Schaffert (sschaffert@apache.org)
 */
public class RelatednessGraph extends UndirectedMutableGraph<Suggestion,Double> {

    public RelatednessGraph() {
        super();
    }

    /**
     * Compute the indegree centrality value of a node as described in <a href="http://www.cse.unt.edu/~rada/papers/sinha.ieee07.pdf">this paper</a>
     * @param v the vertice to calculate the degree for
     * @return
     */
    public double computeCentrality(Suggestion v) {
        double result = 0.0;
        if(getAdjacencyList().get(v) != null) {
            for(Suggestion w : getAdjacencyList().get(v)) {
                Double f1 = getEdge(v, w);
                if(f1 != null) {
                    result += f1;
                }

                // this would only be needed if the graph was directed
                /*
                Double f2 = getEdge(w, v);
                if(f2 != null) {
                    result += f2;
                }
                */
            }
        }
        return result;
    }



}
