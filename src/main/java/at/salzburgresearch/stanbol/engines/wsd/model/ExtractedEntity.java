/*
 * Licensed to the Apache Software Foundation (ASF) under one or more
 * contributor license agreements.  See the NOTICE file distributed with
 * this work for additional information regarding copyright ownership.
 * The ASF licenses this file to You under the Apache License, Version 2.0
 * (the "License"); you may not use this file except in compliance with
 * the License.  You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package at.salzburgresearch.stanbol.engines.wsd.model;

import org.apache.clerezza.rdf.core.LiteralFactory;
import org.apache.clerezza.rdf.core.Triple;
import org.apache.clerezza.rdf.core.TripleCollection;
import org.apache.clerezza.rdf.core.UriRef;
import org.apache.stanbol.enhancer.servicesapi.helper.EnhancementEngineHelper;
import org.apache.stanbol.enhancer.servicesapi.rdf.Properties;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;


import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import static org.apache.stanbol.enhancer.servicesapi.rdf.Properties.*;

public final class ExtractedEntity implements Comparable<ExtractedEntity> {
    private static final Logger log = LoggerFactory.getLogger(ExtractedEntity.class);

    /**
     * The {@link org.apache.clerezza.rdf.core.LiteralFactory} used to create typed RDF literals
     */
    private final static LiteralFactory literalFactory = LiteralFactory.getInstance();
    private String name;
    private UriRef type;
    private UriRef uri;
    private String context;
    private int start;
    private int end;

    /**
     * Map with the suggestion. The key is the URI of the fise:EntityAnnotation and the value is the Triple
     * with the confidence value
     */
    private Map<UriRef,Suggestion> suggestions = new LinkedHashMap<UriRef,Suggestion>();


    /**
     * private constructor only used by {@link #createFromTextAnnotation(org.apache.clerezza.rdf.core.TripleCollection, org.apache.clerezza.rdf.core.UriRef)}
     */
    private ExtractedEntity() {}

    /**
     * creates a ExtractedEntity instance for the parsed fise:TextAnnotation
     *
     * @param graph
     *            the graph with the information
     * @param textAnnotation
     *            the fise:TextAnnotation
     * @return the {@link ExtractedEntity} or <code>null</code> if the parsed text annotation is missing required
     *         information.
     */
    public static ExtractedEntity createFromTextAnnotation(TripleCollection graph, UriRef textAnnotation) {
        ExtractedEntity entity = new ExtractedEntity();
        entity.uri = textAnnotation;
        entity.name = EnhancementEngineHelper.getString(graph, textAnnotation, ENHANCER_SELECTED_TEXT);
        if (entity.name == null) {
            log.debug("Unable to create ExtractedEntity for TextAnnotation {} "
                    + "because property {} is not present", textAnnotation, ENHANCER_SELECTED_TEXT);
            return null;
        }
        // NOTE rwesten: I think one should not change the selected text
        // remove punctuation form the search string
        // entity.name = cleanupKeywords(name);
        if (entity.name.isEmpty()) {
            log.debug("Unable to process TextAnnotation {} because its selects " + "an empty Stirng !",
                textAnnotation);
            return null;
        }
        entity.type = EnhancementEngineHelper.getReference(graph, textAnnotation, DC_TYPE);
        // NOTE rwesten: TextAnnotations without dc:type should be still OK
        // if (type == null) {
        // log.warn("Unable to process TextAnnotation {} because property {}"
        // + " is not present!",textAnnotation, DC_TYPE);
        // return null;
        // }
        entity.context = EnhancementEngineHelper.getString(graph, textAnnotation, ENHANCER_SELECTION_CONTEXT);
        Integer start =
                EnhancementEngineHelper.get(graph, textAnnotation, ENHANCER_START, Integer.class,
                    literalFactory);
        Integer end =
                EnhancementEngineHelper.get(graph, textAnnotation, ENHANCER_END, Integer.class,
                    literalFactory);
        if (start == null || end == null) {
            log.debug("Unable to process TextAnnotation {} because the start and/or the end "
                    + "position is not defined (selectedText: {}, start: {}, end: {})", new Object[] {
                    textAnnotation, entity.name, start, end});

        }
        entity.start = start;
        entity.end = end;

        // parse the suggestions

        // all the entityhubSites that manage a suggested Entity
        // (hopefully only a single one)
        List<Suggestion> suggestionList = new ArrayList<Suggestion>();
        Iterator<Triple> suggestions = graph.filter(null, Properties.DC_RELATION, textAnnotation);
        // NOTE: this iterator will also include dc:relation between fise:TextAnnotation's
        // but in those cases NULL will be returned as suggestion
        while (suggestions.hasNext()) {
            UriRef entityAnnotation = (UriRef) suggestions.next().getSubject();
            Suggestion suggestion = Suggestion.createFromEntityAnnotation(graph, entity, entityAnnotation);
            if (suggestion != null) {
                suggestionList.add(suggestion);
            }
        }
        if (suggestionList.isEmpty()) {
            log.warn("TextAnnotation {} (selectedText: {}, start: {}) has no" + "suggestions.", new Object[] {
                    entity.uri, entity.name, entity.start});
            return null; // nothing to disambiguate
        } else {
            Collections.sort(suggestionList); // sort them based on confidence
            // the LinkedHashMap will keep the order (based on the original
            // confidence)
            for (Suggestion suggestion : suggestionList) {
                entity.suggestions.put(suggestion.getEntityUri(), suggestion);
            }
        }
        return entity;
    }

    /**
     * Removes punctuation form a parsed string
     */
    private static String cleanupKeywords(String keywords) {
        return keywords.replaceAll("\\p{P}", " ").trim();
    }

    /**
     * Getter for the name
     * 
     * @return the name
     */
    public final String getName() {
        return name;
    }

    /**
     * Getter for the type
     * 
     * @return the type
     */
    public final UriRef getType() {
        return type;
    }

    @Override
    public int hashCode() {
        return uri.hashCode();
    }

    @Override
    public boolean equals(Object o) {
        return o instanceof ExtractedEntity && uri.equals(((ExtractedEntity) o).uri);
    }

    @Override
    public String toString() {
        return String.format("ExtractedEntity %s (name=%s | type=%s)", uri, name, type);
    }

    public UriRef getUri() {
        return this.uri;
    }

    public String getContext() {
        return this.context;
    }

    public int getStart() {
        return this.start;
    }

    public int getEnd() {
        return this.end;
    }

    public Collection<Suggestion> getSuggestions() {
        return suggestions.values();
    }

    public Suggestion getSuggestion(UriRef uri) {
        return suggestions.get(uri);
    }

    @Override
    public int compareTo(ExtractedEntity o) {
        int mypos  = (start + end) / 2;
        int hispos = (o.start + o.end) / 2;

        if(mypos == hispos) {
            return this.start - o.start;
        } else {
            return mypos - hispos;
        }
    }
}
