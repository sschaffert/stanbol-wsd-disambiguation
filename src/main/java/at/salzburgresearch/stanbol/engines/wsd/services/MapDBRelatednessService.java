package at.salzburgresearch.stanbol.engines.wsd.services;

import at.salzburgresearch.stanbol.engines.wsd.api.RelatednessService;
import at.salzburgresearch.stanbol.engines.wsd.model.Suggestion;
import org.apache.commons.io.FilenameUtils;
import org.apache.felix.scr.annotations.Activate;
import org.apache.felix.scr.annotations.Component;
import org.apache.felix.scr.annotations.ConfigurationPolicy;
import org.apache.felix.scr.annotations.Deactivate;
import org.apache.felix.scr.annotations.Property;
import org.apache.felix.scr.annotations.Service;
import org.apache.stanbol.enhancer.servicesapi.EnhancementEngine;
import org.mapdb.DB;
import org.mapdb.DBMaker;
import org.osgi.service.cm.ConfigurationException;
import org.osgi.service.component.ComponentContext;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.File;
import java.util.Dictionary;
import java.util.Map;

/**
 * Add file description here!
 *
 * @author Sebastian Schaffert (sschaffert@apache.org)
 */
@Component(
        configurationFactory = true,
        policy = ConfigurationPolicy.REQUIRE,
        specVersion = "1.1",
        metatype = true,
        immediate = true,
        inherit = true)
@Service
@org.apache.felix.scr.annotations.Properties(value = {@Property(name = RelatednessService.PROPERTY_NAME, value = "relatedness-db")})
public class MapDBRelatednessService implements RelatednessService {

    @Property(intValue = 5000)
    public static final String CACHE_SIZE = "org.apache.stanbol.enhancer.engines.wsd_disambiguation.reldb.cache_size";


    @Property(value = "")
    public static final String DB_FILE = "org.apache.stanbol.enhancer.engines.wsd_disambiguation.reldb.database_file";


    private static Logger log = LoggerFactory.getLogger(MapDBRelatednessService.class);


    private File reldbfile;


    /**
     * Relatedness database; stores relatedness using keys of the form uri1.hashCode()*uri2.hashCode() in a treemap called
     * "relatedness"
     */
    private DB reldb;


    /**
     * Relatedness map; stores relatedness using keys of the form uri1.hashCode()*uri2.hashCode()
     */
    private Map<Integer, Double> relmap;

    @Activate
    protected void activate(ComponentContext context) throws ConfigurationException {
        Dictionary<String,Object> config = context.getProperties();

        // need to initialise file from configuration
        reldbfile = null;

        if(config.get(DB_FILE) != null) {
            String fileName = (String) config.get(DB_FILE);

            reldbfile = new File(fileName);
            if(!reldbfile.isAbsolute()) {
                reldbfile = new File(new File(context.getBundleContext().getProperty("sling.home" + File.separator + "wsd")), fileName);
            }
        }


        int cacheSize = 5000;
        if(config.get(CACHE_SIZE) != null) {
            cacheSize = Integer.parseInt(config.get(CACHE_SIZE).toString());
        }


        reldb = DBMaker
                .newFileDB(reldbfile)
                .compressionEnable()
                .closeOnJvmShutdown()
                .cacheLRUEnable()
                .cacheSize(cacheSize)
                .readOnly()
                .transactionDisable()
                .make();

        relmap = reldb.getTreeMap("relatedness");

    }

    @Deactivate
    protected void deactivate(ComponentContext context) {
        log.info("closing MapDB database ...");
        if(reldb != null) {
            reldb.close();
        }
    }


    /**
     * Return the relatedness between the two concepts identified by conceptUri1 and conceptUri2 as a value between
     * 0 and 1. Higher values mean higher relatedness, a value of 0 means "not related at all".
     *
     *
     * @param conceptUri1 URI of the first concept
     * @param conceptUri2 URI of the second concept
     * @return a value between 0 and 1 representing how related the two concepts are
     */
    @Override
    public double getRelatedness(Suggestion conceptUri1, Suggestion conceptUri2) {
        Double d = relmap.get(conceptUri1.getEntityUri().getUnicodeString().hashCode() * conceptUri2.getEntityUri().getUnicodeString().hashCode());
        if(d != null) {
            return d;
        } else {
            return 0.0;
        }
    }
}
