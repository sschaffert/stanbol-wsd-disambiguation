package at.salzburgresearch.stanbol.engines.wsd.util;

import org.mapdb.DB;
import org.mapdb.DBMaker;
import org.supercsv.cellprocessor.ParseDouble;
import org.supercsv.cellprocessor.constraint.StrRegEx;
import org.supercsv.cellprocessor.ift.CellProcessor;
import org.supercsv.exception.SuperCsvConstraintViolationException;
import org.supercsv.io.CsvBeanReader;
import org.supercsv.prefs.CsvPreference;

import javax.management.relation.Relation;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.Map;

/**
 * Utility class to create a MapDB relatedness database from a CSV file.
 *
 * @author Sebastian Schaffert (sschaffert@apache.org)
 */
public class CreateMapDBUtil {


    private File csvFile;

    private File dbFile;

    private CsvPreference preference;

    private Mode mode;

    private CellProcessor[] processors;

    private int invalid = 0;

    public CreateMapDBUtil(File csvFile, File dbFile, Format format, Mode mode) {
        this.csvFile = csvFile;
        this.dbFile = dbFile;

        CsvPreference.Builder prefBuilder;
        switch (format) {
            case SPOTLIGHT:
                prefBuilder = new CsvPreference.Builder('\'', ';',"\r\n");
                break;
            case STANDARD:
            default:
                prefBuilder = new CsvPreference.Builder(CsvPreference.STANDARD_PREFERENCE);
                break;

        }

        this.preference = prefBuilder.build();

        this.mode = mode;

        this.processors = new CellProcessor[] {
                new StrRegEx("^http:.*"),
                new StrRegEx("^http:.*"),
                new ParseDouble()
        };
    }

    public void createDB() throws IOException {
        CsvBeanReader csvReader = new CsvBeanReader(new FileReader(csvFile), this.preference);


        DB reldb = DBMaker
                .newFileDB(dbFile)
                .closeOnJvmShutdown()
                .compressionEnable()
                .transactionDisable()
                .make();

        System.out.println("reading CSV entries ...");
        try {
            Map<Integer,Double> relmap = reldb.getTreeMap("relatedness");

            int count = 0;

            Relation r;
            while( (r = readNext(csvReader)) != null) {
                if(r != null) {
                    if(mode == Mode.RELATEDNESS) {
                        relmap.put(r.getHash(), r.getRelatedness());
                    } else {
                        relmap.put(r.getHash(), 1.0 - r.getRelatedness());
                    }
                }
                count++;

                if(count % 1000 == 0) {
                    System.out.println(count);
                }
            }

            System.out.println("committing database ...");
            reldb.commit();
            System.out.println("compacting database ...");
            reldb.compact();
        } finally {
            reldb.close();
            csvReader.close();
        }

        if(invalid > 0) {
            System.err.println(invalid + " invalid entries in CSV  file");
        }
    }


    private Relation readNext(CsvBeanReader csvReader) throws IOException {
        try {
            return csvReader.read(Relation.class, new String[] {"concept1", "concept2", "relatedness"}, processors);
        } catch (SuperCsvConstraintViolationException ex) {
            invalid++;

            return readNext(csvReader);
        }
    }


    public static void main(String[] args) throws IOException {
        if(args.length != 2) {
            System.err.println("usage: CreateMapDBUtil input.csv output.db");
        } else {
            String infilename  = args[0];
            String outfilename = args[1];

            System.out.println("creating database file " + outfilename + " (input="+infilename+")");

            File infile  = new File(infilename);
            File outfile = new File(outfilename);

            CreateMapDBUtil util = new CreateMapDBUtil(infile,outfile,Format.SPOTLIGHT, Mode.DISTANCE);
            util.createDB();
        }
    }


    private static enum Format {
        STANDARD, SPOTLIGHT
    }

    private static enum Mode {
        RELATEDNESS, DISTANCE
    }


    public static class Relation {
        private String concept1, concept2;
        private double relatedness;

        public Relation() {
        }

        public String getConcept1() {
            return concept1;
        }

        public void setConcept1(String concept1) {
            this.concept1 = concept1;
        }

        public String getConcept2() {
            return concept2;
        }

        public void setConcept2(String concept2) {
            this.concept2 = concept2;
        }

        public double getRelatedness() {
            return relatedness;
        }

        public void setRelatedness(double relatedness) {
            this.relatedness = relatedness;
        }

        public void setRelatedness(String relatedness) {
            try {
                this.relatedness = Double.parseDouble(relatedness);
            } catch (NumberFormatException ex) {
                this.relatedness = 0.0;
            }
        }

        public Integer getHash() {
            return concept1.hashCode() * concept2.hashCode();
        }
    }

}
