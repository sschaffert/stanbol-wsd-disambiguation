package at.salzburgresearch.stanbol.engines.wsd.services;

import at.salzburgresearch.stanbol.engines.wsd.api.RelatednessService;
import at.salzburgresearch.stanbol.engines.wsd.model.Suggestion;
import org.apache.commons.lang.StringUtils;

/**
 * Compute relatedness based on Levenstein distance between the string representations of the URIs. Mostly useful for
 * testing purposes, as this does not reflect any semantic similarity.
 *
 * @author Sebastian Schaffert (sschaffert@apache.org)
 */
public class LevensteinRelatednessService implements RelatednessService {

    public LevensteinRelatednessService() {
    }

    /**
     * Return the relatedness between the two concepts identified by conceptUri1 and conceptUri2 as a value between
     * 0 and 1. Higher values mean higher relatedness, a value of 0 means "not related at all".
     *
     *
     * @param conceptUri1 URI of the first concept
     * @param conceptUri2 URI of the second concept
     * @return a value between 0 and 1 representing how related the two concepts are
     */
    @Override
    public double getRelatedness(Suggestion conceptUri1, Suggestion conceptUri2) {
        return 1 / (1 + StringUtils.getLevenshteinDistance(conceptUri1.getEntityUri().getUnicodeString(), conceptUri2.getEntityUri().getUnicodeString()));
    }
}
