package at.salzburgresearch.stanbol.engines.wsd.services;

import at.salzburgresearch.stanbol.engines.wsd.api.RelatednessService;
import at.salzburgresearch.stanbol.engines.wsd.model.Suggestion;
import com.google.common.cache.CacheBuilder;
import com.google.common.cache.CacheLoader;
import com.google.common.cache.LoadingCache;
import org.apache.felix.scr.annotations.Activate;
import org.apache.felix.scr.annotations.Component;
import org.apache.felix.scr.annotations.ConfigurationPolicy;
import org.apache.felix.scr.annotations.Property;
import org.apache.felix.scr.annotations.Service;
import org.apache.stanbol.enhancer.servicesapi.EnhancementEngine;
import org.json.JSONException;
import org.json.JSONObject;
import org.json.JSONTokener;
import org.osgi.service.cm.ConfigurationException;
import org.osgi.service.component.ComponentContext;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLConnection;
import java.net.URLDecoder;
import java.net.URLEncoder;
import java.util.Dictionary;
import java.util.concurrent.TimeUnit;

/**
 * This implements the Normalized Google Distance (NGD) as described in
 * R.L. Cilibrasi and P.M.B. Vitanyi, "The Google Similarity Distance",
 * IEEE Trans. Knowledge and Data Engineering, 19:3(2007), 370 - 383
 * <p/>
 * copied from http://code.google.com/p/ext-c/source/browse/trunk/src/nz/ac/vuw/ecs/kcassell/similarity/GoogleDistanceCalculator.java
 *
 * @author Sebastian Schaffert (sschaffert@apache.org)
 */
@Component(
        configurationFactory = true,
        policy = ConfigurationPolicy.OPTIONAL,
        specVersion = "1.1",
        metatype = true,
        immediate = true,
        inherit = true)
@Service
@org.apache.felix.scr.annotations.Properties(value = {@Property(name = RelatednessService.PROPERTY_NAME, value = "relatedness-google")})
public class GoogleRelatednessService implements RelatednessService {

    private static Logger logger = LoggerFactory.getLogger(GoogleRelatednessService.class);

    @Property(intValue = 5000)
    public static final String CACHE_SIZE = "org.apache.stanbol.enhancer.engines.wsd_disambiguation.ngd.cache_size";


    /** A Google URL that will return the number of matches, among other things. */
    private static final String GOOGLE_SEARCH_SITE_PREFIX = "http://ajax.googleapis.com/ajax/services/search/web?v=1.0&";

    /** The logarithm of a number that is (hopefully) greater than or equal
     *  to the (unpublished) indexed number of Google documents.
     *  http://googleblog.blogspot.com/2008/07/we-knew-web-was-big.html
     *  puts this at a trillion or more.  */
    protected final static double logN = Math.log(1.0e12);


    protected LoadingCache<String, Double> cache;



    @Activate
    protected void activate(ComponentContext context) throws ConfigurationException {
        Dictionary<String,Object> config = context.getProperties();

        int cacheSize = 5000;
        if(config.get(CACHE_SIZE) != null) {
            cacheSize = Integer.parseInt(config.get(CACHE_SIZE).toString());
        }

        cache = CacheBuilder.newBuilder()
                .maximumSize(cacheSize)
                .expireAfterAccess(1, TimeUnit.DAYS)
                .build(new CacheLoader<String, Double>() {
                    @Override
                    public Double load(String key) throws Exception {
                        URL url = null;
                        InputStream stream = null;
                        try {
                            url = makeQueryURL(key);
                            URLConnection connection = url.openConnection();
                            stream = connection.getInputStream();
                            InputStreamReader inputReader = new InputStreamReader(stream);
                            BufferedReader bufferedReader = new BufferedReader(inputReader);
                            double d = getCountFromQuery(bufferedReader);
                            logger.debug("retrieved weight for term '{}': {}", key,d);
                            return d;
                        }
                        finally {
                            if (stream != null) {
                                try {
                                    stream.close();
                                } catch (IOException e) {
                                    logger.warn(e.toString());
                                }
                            }
                        }

                    }
                });
    }


    private double getCountFromQuery(BufferedReader bufferedReader) throws JSONException {
        JSONObject json = new JSONObject(new JSONTokener(bufferedReader));
        JSONObject responseData = json.getJSONObject("responseData");
        JSONObject cursor = responseData.getJSONObject("cursor");
        double count = 0;

        try {
            count = cursor.getDouble("estimatedResultCount");
        } catch (JSONException e) {
            // exception will be thrown when no matches are found
            count = 0;
        }
        return count;
    }

    protected URL makeQueryURL(String term) throws MalformedURLException, IOException {
        //String searchTerm = term.replaceAll(" ", "+");
        String searchTerm = URLEncoder.encode(term, "UTF-8");
        URL url;
              String urlString = makeGoogleQueryString(searchTerm);
        url = new URL(urlString);
        return url;
    }

    /**
     * Builds a query string suitable for Google
     * @param searchTerm
     * @return
     */
    private String makeGoogleQueryString(String searchTerm) {
        String urlString = GOOGLE_SEARCH_SITE_PREFIX + "q=" + searchTerm + " ";
                /*
                 * Example queries:
                        cassell: q=cassell
                        keith cassell: q=keith+cassell
                        "keith cassell": q=%22keith+cassell%22
                        "keith cassell" betweenness: q=%22keith+cassell%22+betweenness
                 */
        return urlString;
    }


    /**
     * Calculates the normalized Google Distance (NGD) between the two terms
     * specified.  NOTE: this number can change between runs, because it is
     * based on the number of web pages found by Google, which changes.
     * @return a number from 0 (minimally distant) to 1 (maximally distant),
     *   unless an exception occurs in which case, it is negative
     *   (RefactoringConstants.UNKNOWN_DISTANCE)
     */
    public Double calculateDistance(String term1, String term2) {
        double distance = -1.0;

        try {
            double min = cache.get(term1);
            double max = cache.get(term2);
            double both = cache.get(term1 + " " + term2);

            // if necessary, swap the min and max
            if (max < min) {
                double temp = max;
                max = min;
                min = temp;
            }

            if (min > 0.0 && both > 0.0) {
                distance =
                        (Math.log(max) - Math.log(both)) / (logN - Math.log(min));
            } else {
                distance = 1.0;
            }

            // Counts change and are estimated, so there would be a possibility
            // of a slightly negative distance.
            if (distance < 0.0) {
                distance = 0.0;
            }
        } catch (Exception e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        return distance;
    }

    /**
     * Return the relatedness between the two concepts identified by conceptUri1 and conceptUri2 as a value between
     * 0 and 1. Higher values mean higher relatedness, a value of 0 means "not related at all".
     *
     *
     * @param conceptUri1 URI of the first concept
     * @param conceptUri2 URI of the second concept
     * @return a value between 0 and 1 representing how related the two concepts are
     */
    @Override
    public double getRelatedness(Suggestion conceptUri1, Suggestion conceptUri2) {
        // there are different approaches for getting the entity labels; the cheapest one is to extract it from the
        // URI itself (this one is implemented below); a more sophisticated approach would look up the entity label
        // in the entityhub


        return 1.0 - calculateDistance(conceptUri1.getLabel(), conceptUri2.getLabel());
    }


    public String getLabel(String uri) {
        // the basic approach is:
        // 1. cut away everything up to the last occurrence of / from each URI
        // 2. URI-decode the remaining string
        // 3. replace occurrences of _ and - with spaces
        String label = uri.substring(uri.lastIndexOf("/") + 1);

        try {
            label = URLDecoder.decode(label, "UTF-8");
        } catch (UnsupportedEncodingException e) {
        }
        label = label.replaceAll("(-|_)"," ");

        return label;
    }

    public static void main(String[] args) throws IOException {
        String uri1 = "http://dbpedia.org/resource/Paris";
        String uri2 = "http://dbpedia.org/resource/France";
        String uri3 = "http://dbpedia.org/resource/Texas";
        String uri4 = "http://dbpedia.org/resource/Paris_Hilton";

        GoogleRelatednessService s = new GoogleRelatednessService();

        System.err.println("uri1: " + s.getLabel(uri1));
        System.err.println("uri2: " + s.getLabel(uri2));
        System.err.println("uri3: " + s.getLabel(uri3));
        System.err.println("uri4: " + s.getLabel(uri4));
        System.err.println("distance (Paris,France): " + s.calculateDistance(s.getLabel(uri1), s.getLabel(uri2)));
        System.err.println("distance (Paris,Texas): " + s.calculateDistance(s.getLabel(uri1), s.getLabel(uri3)));
        System.err.println("distance (Paris,Hilton): " + s.calculateDistance(s.getLabel(uri1), s.getLabel(uri4)));
    }
}
